import Link from 'next/link';
import Image from 'next/Image';

import styles from '../assets/styles/index.module.css'

const goImage = require('../assets/images/pant.png')
function Index() {

    return (
        <div className={styles.pageContainer}>
            <Image src={goImage} alt="Picture of the author" className={styles.imageContainer} />

            <Link href="/posts" ><a className={styles.customLink}><h1>See all posts &#8594;</h1></a></Link>
        </div>

    )
}

export default Index;