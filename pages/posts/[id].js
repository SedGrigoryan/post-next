import { useRouter } from 'next/router';
import { useState } from 'react'
import PostItem from '../../components/PostItem/PostItem'
import CommentItem from '../../components/CommentItem/CommentItem';
import LoadingButton from '@mui/lab/LoadingButton';
import styles from '../../assets/styles/post.module.css'
function PostPage({ post, comments, page }) {
    const limit = 3;
    const [postComments, setPostComments] = useState(comments)
    const [pageCount, setPageCount] = useState(page)
    const [loading, setLoading] = useState(false);
    const [disable, setDisable] = useState(false)
    const router = useRouter();

    const showMore = async () => {
        setLoading(true)
        const response = await fetch(`http://jsonplaceholder.typicode.com/posts/${router.query.id}/comments?_page=${pageCount + 1}&_limit=${limit}`)
        const data = await response.json()
        setPageCount(pageCount + 1)
        setLoading(false);

        let _data = [...postComments, ...data]
        if (data.length < limit) {
            setDisable(true)
        }
        setPostComments(_data)
    }

    return (
        <div className={styles.postPageContainer}>

            {post && (
                <div className={styles.postPageWrapper}>
                    <div className={styles.postPageItem}>
                        <PostItem post={post} />
                    </div>
                    <em><p>Comments</p></em>
                    {
                        postComments?.length ?
                            <div>
                                {postComments.map((comment, index) =>
                                    <CommentItem comment={comment} key={comment.id} />
                                )}
                                <LoadingButton
                                    className={styles.loadingButton}
                                    loading={loading}
                                    disabled={disable}
                                    loadingIndicator="Loading..."
                                    variant="outlined"
                                    onClick={showMore}>
                                    Show More
                                </LoadingButton>
                            </div>

                        : <p>Ooops no comments :(</p>
                    }
                </div>
            )}
        </div>
    )
}

PostPage.getInitialProps = async ({ query: { page = 1, id }}) => {
    const comment = await fetch(`http://jsonplaceholder.typicode.com/posts/${id}/comments?_page=${page}&_limit=3`)
    const post = await fetch(`http://jsonplaceholder.typicode.com/posts/${id}`)
    const commentData = await comment.json();
    const postData = await post.json();
    return {
        comments: commentData,
        post: postData,
        page: page
    }
}

export default PostPage;