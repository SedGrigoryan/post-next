import Link from 'next/link';
import { useRouter } from 'next/router';
import fetch from 'isomorphic-fetch';
import { Button } from '@mui/material';
import PostItem from '../components/PostItem/PostItem'
import styles from '../assets/styles/posts.module.css'
function Posts({ posts, page }) {
    const router = useRouter();

    const getPage = (page) => {
        router.push({
            query: { page }
        })
    }
    const isFirstPage = page === 1;

    return (
        <div className={styles.postContainer} >
            <h1>Posts</h1>
            {
                posts && posts.length > 0 ? (
                    <div >
                        <div className={styles.postsContainer}>
                            {posts.map((item) => {
                                return (
                                    <div key={item.id} className={styles.itemWrapper}>
                                        <Link href={`/posts/[id]`} as={`/posts/${item.id}`} >
                                            <a><PostItem post={item} /> </a>
                                        </Link>
                                    </div>

                                )
                            })
                            }
                        </div>

                        <div className={styles.buttonsWrapper}>
                            {
                                !isFirstPage == 1 ? (
                                    <div>
                                        <Button onClick={() => getPage(parseInt(page) - 1)}>Previous</Button>
                                        <Button onClick={() => getPage(parseInt(page) + 1)}>Next</Button>
                                    </div>
                                ) : (
                                    <Button onClick={() => getPage(parseInt(page) + 1)}>Next</Button>
                                )
                            }
                        </div>
                    </div>
                ) : (
                    <h1>No posts yet...</h1>
                )
            }


        </div>
    )
}

Posts.getInitialProps = async ({ query: { page = 1 } }) => {
    const response = await fetch(`http://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=10`)
    const data = await response.json();

    return {
        posts: data,
        page: page,
    };
}
export default Posts;