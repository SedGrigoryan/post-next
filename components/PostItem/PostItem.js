import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

import styles from './PostItem.module.css'

const PostItem = ({ post }) => {
    return (
        <Card sx={{ maxWidth: '100%' }} className={styles.cardContainer} >
            <CardActionArea>
                <CardContent>
                    <Typography gutterBottom  component="div" className={styles.titleWrapper}>
                       <h4> {post.title}</h4>
                    </Typography>
                    <Typography variant="body2" color="text.secondary" className={styles.bodyWrapper}>
                        {post.body}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    )
}

export default PostItem;