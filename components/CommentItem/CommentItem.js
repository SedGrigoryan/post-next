import React from 'react';
import Card from '@mui/material/Card';
import Avatar from '@mui/material/Avatar';

import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

import styles from './CommentItem.module.css';

const CommentItem = ({ comment }) => {
    return (
        <div className={styles.commentItem}>
            <Card sx={{ maxWidth: '100%' }}>
                <CardActionArea>
                    <CardContent>
                        <Typography variant="body2" component="div" className={styles.mailWrapper}>
                            <Avatar className={styles.customAvatar}>{comment.email[0].toUpperCase()}</Avatar>
                            <p>{comment.email}</p>
                        </Typography>
                        <Typography gutterBottom component="div">
                            <h4> {comment.name}</h4>
                        </Typography>
                        <Typography variant="body2" component="div">
                            <p>
                                {comment.body}
                            </p>
                        </Typography>

                    </CardContent>
                </CardActionArea>
            </Card>
        </div>

    )
}

export default CommentItem;